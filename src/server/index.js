let matches = require('../data/matches.json');
let deliveries = require('../data/deliveries.json');
const fs = require('fs');
const iplFunctions = require('./ipl');

const iplYearBook = iplFunctions.matchesPerYear(matches);
filePath1 = "../public/output/matchesPerYear.json"
writeJson(filePath1,iplYearBook);

const iplResultBook = iplFunctions.noOfWinsPerYear(matches);
filePath2 = "../public/output/noOfWinsPerYear.json"
writeJson(filePath2,iplResultBook);

const extraRuns = iplFunctions.extraRuns2016(deliveries, matches)
filePath3 = "../public/output/extraRuns2016.json"
writeJson(filePath3,extraRuns);

const economyBowlers = iplFunctions.economicalBowlers(deliveries,matches)
filePath4 = "../public/output/economicalBowlers.json"
writeJson(filePath4,economyBowlers);

const matchWonAndTossWon = iplFunctions.wonTossAndMatch(matches)
filePath5 = "../public/output/matchWonAndTossWon.json"
writeJson(filePath5,matchWonAndTossWon);

const runsByKohli = iplFunctions.runsByKohliIn2016(matches, deliveries)
filePath6 = "../public/output/runsByKohli.json"
writeJson(filePath6,runsByKohli);

const bestSuperOverEco = iplFunctions.superOverEconomy(deliveries)
filePath7 = "../public/output/bestSuperOverEco.json"
writeJson(filePath7,bestSuperOverEco);

const manOfTheMatch = iplFunctions.playerOfTheMatch(matches)
filePath8 = "../public/output/manOfTheMatch.json"
writeJson(filePath8,manOfTheMatch);

function writeJson(filePath,FileName){
   fs.writeFile(filePath, JSON.stringify(FileName), (err) => {
      if (err) console.log(err)
   })
   }
   