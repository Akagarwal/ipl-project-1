let matches = require('../data/matches.json');
let deliveries = require('../data/deliveries.json');

const fs = require('fs');



//Number of matches played per year for all the years in IPL.
function matchesPerYear(matches) {
  let iplMatchesPerYear = matches.reduce((iplMatchesPerYear, currentValue) => {
    if (iplMatchesPerYear.hasOwnProperty(currentValue.season)) {
      iplMatchesPerYear[currentValue.season] += 1;
    } else {
      iplMatchesPerYear[currentValue.season] = 1;
    }
    return iplMatchesPerYear;
  }, {})
  return iplMatchesPerYear;
}



//Number of matches won per team per year in IPL.
function noOfWinsPerYear(matches) {
  let iplMatcheResults = matches.reduce((iplMatcheResults, currentMatch) => {
    if (iplMatcheResults.hasOwnProperty(currentMatch.winner)) {
      if (iplMatcheResults[currentMatch.winner].hasOwnProperty(currentMatch.season)) {
        iplMatcheResults[currentMatch.winner][currentMatch.season] += 1;
      } else {
        if (currentMatch.winner != '') {
          iplMatcheResults[currentMatch.winner][currentMatch.season] = 1;
        }
      }
    }
    else {
      iplMatcheResults[currentMatch.winner] = {};
      iplMatcheResults[currentMatch.winner][currentMatch.season] = 1
    }
    return iplMatcheResults;
  }, {});

  return iplMatcheResults;
}



//Extra runs conceded per team in the year 2016
function extraRuns2016(deliveries, matches) {

  let idOf2016 = matches.filter((match) => match.season == 2016)
    .map((match) => parseInt(match.id));

  let extraRuns = deliveries.reduce((extraRuns, currentBall) => {
    let matchId = parseInt(currentBall.match_id);
    if (idOf2016.includes(matchId)) {
      if (extraRuns.hasOwnProperty(currentBall.bowling_team)) {
        extraRuns[currentBall.bowling_team] += parseInt(currentBall.extra_runs);
      } else {
        extraRuns[currentBall.bowling_team] = parseInt(currentBall.extra_runs);
      }
    }
    return extraRuns;
  }, {})
  return extraRuns;
}



//Top 10 economical bowlers in the year 2015:
function economicalBowlers(deliveries, matches) {

  let idOf2015 = matches.filter((match) => match.season == 2015)
    .map((match) => parseInt(match.id));

  let allBowlers = deliveries.reduce((allBowlers, currentBall) => {

    let matchId = parseInt(currentBall.match_id)
    if (idOf2015.includes(matchId)) {
      if (allBowlers.hasOwnProperty(currentBall.bowler)) {
        allBowlers[currentBall.bowler].balls += 1;
        allBowlers[currentBall.bowler].runs += parseInt(currentBall.total_runs);
        let run = allBowlers[currentBall.bowler].runs;
        let ball = allBowlers[currentBall.bowler].balls;
        allBowlers[currentBall.bowler].economy = Number((run / (ball / 6)).toFixed(2));
      } else {
        allBowlers[currentBall.bowler] = { "runs": parseInt(currentBall.total_runs), "balls": 1, "economy": 0 };
      }
    }
    return allBowlers;
  }, {})
  const onlyEconomy = [];
  for (let bowlerName in allBowlers) {
    if (allBowlers[bowlerName].economy <= 8) {
      onlyEconomy.push(allBowlers[bowlerName].economy)
    }
  }

  let result = onlyEconomy.sort();

  const top10Bowlers = {}
  for (let i = 0; i < 10; i++) {
    for (key in allBowlers) {
      if (allBowlers[key].economy == onlyEconomy[i]) {
        top10Bowlers[key] = allBowlers[key].economy;
      }
    }
  }
  return top10Bowlers;
};



//Find the number of times each team won the toss and also won the match
function wonTossAndMatch(matches) {

  let resultWonTossAndMatch = matches.reduce((resultWonTossAndMatch, match) => {
    if (resultWonTossAndMatch.hasOwnProperty(match.season)) {
      if (match.toss_winner = match.winner) {
        resultWonTossAndMatch[match.season] += 1;
      }
    } else {
      if (match.toss_winner = match.winner) {
        resultWonTossAndMatch[match.season] = 1;
      } else {
        resultWonTossAndMatch[match.seson] = 0
      }
    }
    return resultWonTossAndMatch;
  }, {});
  return resultWonTossAndMatch;
};



//Calculate runs scored by V Kohli in 2016
function runsByKohliIn2016(matches, deliveries) {

  let idOfMatches2016 = matches.filter((element) => element.season = 2016)
    .map((element) => parseInt(element.id));

  let kohliRuns2016 = deliveries.reduce((kohliRuns2016, match) => {
    let matchId = parseInt(match.match_id);
    if (idOfMatches2016.includes(matchId)) {
      if (match.batsman = "V. Kohli") {
        kohliRuns2016[match.batsman] += parseInt(match.batsman_runs);
      }
    }
    return kohliRuns2016;

  }, { "V. Kohli": 0 })

  return kohliRuns2016;
};



// //Find the bowler with the best economy in super overs
function superOverEconomy(deliveries) {

  let economyBowlers = deliveries.reduce((economyBowlers, match) => {

    if (economyBowlers.hasOwnProperty(match.bowler)) {

      if (match.is_super_over == '1') {
        economyBowlers[match.bowler].balls += 1;
        economyBowlers[match.bowler].runs += parseInt(match.total_runs);
        let run = economyBowlers[match.bowler].runs;
        let ball = economyBowlers[match.bowler].balls;
        economyBowlers[match.bowler].economy = (run / (ball / 6)).toFixed(2);
      }
    }
    else {
      if (match.is_super_over == '1') {
        economyBowlers[match.bowler] = { "bowler": match.bowler, "runs": parseInt(match.total_runs), "balls": 1, "economy": 0 };
      }
    }
    return economyBowlers;
  }, {})
  let economyOnly = Object.values(economyBowlers).sort((a, b) => { return a.economy - b.economy })
  result = economyOnly[0];
  return result;
};



//Find a player who has won the highest number of Player of the Match awards for each season
function playerOfTheMatch(matches) {

  let manOfTheMatches = matches.reduce((manOfTheMatches, match) => {
    if (manOfTheMatches.hasOwnProperty(match.season)) {

      if (manOfTheMatches[match.season].hasOwnProperty(match.player_of_match)) {
        manOfTheMatches[match.season][match.player_of_match] += 1;
      } else {
        manOfTheMatches[match.season][match.player_of_match] = 1;
      }
    }
    else {
      manOfTheMatches[match.season] = {};
    }
    return manOfTheMatches;
  }, {});
  
  let sortedOrderOfAwards = Object.values(manOfTheMatches)
      .map(nameAndNoOfAwards => Object.entries(nameAndNoOfAwards)
      .sort((current, previous) => current[1] - previous[1]));

  const topPlayers = sortedOrderOfAwards.map(nameAwards => nameAwards[nameAwards.length-1]);
 
  let mostAwardsPerYear =  matches.reduce((mostAwardsPerYear, match) => {
    mostAwardsPerYear[match.season] = 0;
    return mostAwardsPerYear;
}, {})

  
  Object.keys(mostAwardsPerYear).forEach((element,index) => {
    let name = topPlayers[index][0];
    let value = topPlayers[index][1];
    mostAwardsPerYear[element] = { [name]: value }
  })
 
  return mostAwardsPerYear;
}

const mostAwardsPerYear = playerOfTheMatch(matches);



module.exports = { matchesPerYear, noOfWinsPerYear, extraRuns2016, economicalBowlers, wonTossAndMatch, runsByKohliIn2016, superOverEconomy, playerOfTheMatch };